const tape = require("tape");
const Gridmap = require("../");

const params = {
  numCols: 3,
  numRows: 4,
  keys: ["AA", "BB", "CC", "DD", "EE"],
  cols: [2, 1, 2, 0, 1],
  rows: [0, 3, 3, 2, 2]
}

tape("Gridmap() constructor creation of a gridmap object", function(test) {
  const g = new Gridmap(params);
  test.equal(g.length, 5);
  test.equal(g.numCols, 3);
  test.equal(g.numRows, 4);
  test.deepEqual(g.keys, ["AA", "BB", "CC", "DD", "EE"]);
  test.deepEqual(g.cols, [2,1,2,0,1]);
  test.deepEqual(g.rows, [0,3,3,2,2]);
  test.end();
});

tape('Exceptions in case of erroneous parameters in the Gridmap() constructor', function(test) {
  /* Tests on numCols parameter */
  test.throws(function(t) {
    const g = new Gridmap({...params, numCols: 2});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, numCols: -1});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, numCols: 4.37});
  }, TypeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, numCols: new Date()});
  }, TypeError)
  /* Tests on numRows parameter */
  test.throws(function(t) {
    const g = new Gridmap({...params, numRows: 2});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, numRows: -1});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, numRows: 4.37});
  }, TypeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, numRows: new Date()});
  }, TypeError)
  /* Tests on keys parameter */
  test.throws(function(t) {
    const g = new Gridmap({...params, keys: {0: "AA", 1: "BB"}});
  }, TypeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, keys: []});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, keys: ["AA", "BB", 3, 4.2, "EE"]});
  }, TypeError)
  /* Tests on cols parameter */
  test.throws(function(t) {
    const g = new Gridmap({...params, cols: {"AA": 2, "BB": 1}});
  }, TypeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, cols: []});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, cols: [2,1,2,0]});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, cols: [2,1,2,0,1.1]});
  }, TypeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, cols: [2,1,-2,0,1]});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, cols: [2,1,2,0,3]});
  }, RangeError)
  /* Tests on rows parameter */
  test.throws(function(t) {
    const g = new Gridmap({...params, rows: {"AA": 2, "BB": 1}});
  }, TypeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, rows: []});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, rows: [0,3,3,2]});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, rows: [0,3,3,2,1.1]});
  }, TypeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, rows: [0,3,-3,2,2]});
  }, RangeError)
  test.throws(function(t) {
    const g = new Gridmap({...params, rows: [0,3,3,4,2]});
  }, RangeError)

  test.end();
});

tape("Test on deep copy of the parameters keys, cols, rows", function(test) {
  const params = {
    numCols: 3,
    numRows: 4,
    keys: ["AA", "BB", "CC", "DD", "EE"],
    cols: [2, 1, 2, 0, 1],
    rows: [0, 3, 3, 2, 2]
  }
  const g = new Gridmap(params);
  test.equal(g.length, 5);
  test.equal(g.numCols, 3);
  test.equal(g.numRows, 4);
  test.deepEqual(g.keys, ["AA", "BB", "CC", "DD", "EE"]);
  test.deepEqual(g.cols, [2,1,2,0,1]);
  test.deepEqual(g.rows, [0,3,3,2,2]);
  params.keys[0] = "FF"
  params.cols[1] = 4
  params.rows[2] = -3
  test.equal(g.length, 5);
  test.equal(g.numCols, 3);
  test.equal(g.numRows, 4);
  test.deepEqual(g.keys, ["AA", "BB", "CC", "DD", "EE"]);
  test.deepEqual(g.cols, [2,1,2,0,1]);
  test.deepEqual(g.rows, [0,3,3,2,2]);
  test.end()
})
