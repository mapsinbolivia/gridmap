# gridmap

Gridmaps are a popular visualization format for showing a map, without the typical area distortion, and with one square for every polygon ([more on cartograms](https://beta.observablehq.com/@severo/grid-cartograms)).

![A typical gridmap, by WNYC](img/gridmap.png)

This JavaScript module provides the constructor for creating gridmaps.

To install it in npm:

```
npm install mapsinbolivia/gridmap
```

To use it from the browser:

```
require('@mapsinbolivia/gridmap@1.0.24/dist/gridmap.js')
```
