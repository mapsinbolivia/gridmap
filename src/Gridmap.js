export default function Gridmap({numCols, numRows, keys, cols, rows}) {
  if (!(Number.isInteger(numCols)))
    throw new TypeError('"numCols" parameter must be an integer - got: ' + numCols.toString())
  else if (numCols < 1)
    throw new RangeError('"numCols" parameter must be a positive integer - got: ' + numCols.toString())
  else
    this.numCols = numCols

  if (!(Number.isInteger(numRows)))
    throw new TypeError('"numRows" parameter must be an integer - got: ' + numRows.toString())
  else if (numRows < 1)
    throw new RangeError('"numRows" parameter must be a positive integer - got: ' + numRows.toString())
  else
    this.numRows = numRows

  if (!(Array.isArray(keys)))
    throw new TypeError('"keys" parameter must be an Array - got: ' + keys.toString())
  if (keys.length === 0)
    throw new RangeError('"keys" Array must not be empty')
  if (!(Array.isArray(cols)))
    throw new TypeError('"cols" parameter must be an Array - got: ' + cols.toString())
  if (cols.length !== keys.length)
    throw new RangeError('"cols" Array length must be the same'
      + ' as "keys" Array ('+keys.length.toString()+') - got: ' + cols.length.toString())
  if (!(Array.isArray(rows)))
    throw new TypeError('"rows" parameter must be an Array - got: ' + rows.toString())
  if (rows.length !== keys.length)
    throw new RangeError('"rows" Array length must be the same'
      + ' as "keys" Array ('+keys.length.toString()+') - got: ' + rows.length.toString())

  /* This has a bad performance: using Benchmark.js, it can be shown that
   * these validations add a 35% extra time.
   * Maybe it could be optimized, or explicitely avoided using an option. */
  keys.forEach(function(e,i) {
    if (typeof e !== 'string')
      throw new TypeError('Element nº'+i.toString()+' of array "keys" is'
        + ' not a string, string expected - got: '+e.toString())
    if (!(Number.isInteger(cols[i])))
      throw new TypeError('Element nº'+i.toString()+' of array "cols" is'
        + ' not an integer, integer expected - got: '+cols[i].toString())
    if (!(Number.isInteger(rows[i])))
      throw new TypeError('Element nº'+i.toString()+' of array "rows" is'
        + ' not an integer, integer expected - got: '+rows[i].toString())
    if (cols[i] < 0 || cols[i] > numCols - 1)
      throw new RangeError('Value of element nº'+i.toString()+' of array "cols"'
        + ' should lay between 0 and '+(numCols-1).toString()+' - got: '+cols[i].toString())
    if (rows[i] < 0 || rows[i] > numRows - 1)
      throw new RangeError('Value of element nº'+i.toString()+' of array "rows"'
        + ' should lay between 0 and '+(numRows-1).toString()+' - got: '+rows[i].toString())
  })
  /* Here we should check if there is no previous (col,row) pair with the
   * same values.
   * Note that it would be add another performance penalty. */
  this.keys = keys.slice();
  this.cols = cols.slice();
  this.rows = rows.slice();
}

Object.defineProperty(Gridmap.prototype, 'length', {
  get() {
    return this.keys.length;
  }
});

Gridmap.prototype.toMatrix = function() {
  let m = new Array(this.numRows).fill().map(function () {return new Array(this.numCols).fill("")})
  this.keys.forEach((e,i) => m[this.rows[i]][this.cols[i]] = e)
  return m
}
